<?php
namespace Deployer;

require 'recipe/laravel.php';

// Config
set('repository', 'git@bitbucket.org:darulfajarr_/backend-rekessim.git');
add('shared_files', ['.env']);
add('shared_dirs', ['storage']);
add('writable_dirs', [    
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
    ]);

// Hosts
host('143.198.80.191')
    ->set('remote_user', 'deploy')
    ->set('deploy_path', '~/apps/be-rkm')
    ->set('branch', 'dev')
    ->set('labels', ['stage' => 'staging'])
    ->set('keep_releases', 5);

// Hooks
after('deploy:failed', 'deploy:unlock');
