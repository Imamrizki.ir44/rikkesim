<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use DB;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validateUser = Validator::make($request->all(), [
            'name'              => 'required',
            'phone'             => 'required|unique:users,phone,NULL,id',
            'password'          => 'required|min:8',
            'password_c'        => 'required|same:password'
        ],[
            'name.required'          => 'Nama lengkap wajib diisi !',
            'phone.required'         => 'Nomor telepon wajib diisi !',
            'phone.unique'           => 'Nomor telepon sudah terdaftar !',
            'password.required'      => 'Password wajib diisi !',
            'password.min'           => 'Password minimal harus 8 karakter kombinasi !',
            'password_c.required'    => 'Konfirmasi pasword wajib diisi !',
            'password_c.same'        => 'Konfirmasi pasword tidak sama !'
        ]);

        if ($validateUser->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validateUser->errors(),
                'data'    => null
            ], 422);
        }

        try {

            DB::beginTransaction();
            
            $userCreate = new User;
            $userCreate->name      = $request->name;
            $userCreate->phone     = $request->phone;
            $userCreate->password  = Hash::make($request->password);
            $userCreate->save();

            $userCreate->assignRole('Pemohon');

            $token = $userCreate->createToken('auth-token')->plainTextToken;

            DB::commit();
            
            return response()->json([
                'success'       => true,
                'message'       => 'Berhasil mendaftar !',
                'data'          => $userCreate,
                'access_token'  => $token,
                'token_type'    => 'Bearer',
            ], 200);

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function login(Request $request)
    {
        $validateUser = Validator::make($request->all(), [
            'phone'             => 'required',
            'password'          => 'required'
        ],[
            'phone.required'         => 'Nomor telepon wajib diisi !',
            'password.required'      => 'Password wajib diisi !'
        ]);

        if ($validateUser->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validateUser->errors(),
                'data'    => null
            ], 422);
        }
        
        $user = User::where('phone', $request->phone)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'success'       => false,
                'message'       => 'Unauthorized',
                'data'          => null,
            ], 401);
        }

        $token = $user->createToken('auth-token')->plainTextToken;
        
        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil masuk !',
            'data'          => $user,
            'access_token'  => $token,
            'token_type'    => 'Bearer',
        ], 200);
    }

    public function forgot(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nik' => [
                'required', function ($attribute, $value, $fail) {
                    if (!User::where('nik', '=', $value)->exists()) {
                        $fail('NIK yang Anda masukan tidak terdaftar !');
                    }
                },
            ],
            'phone' => [
                'required', function ($attribute, $value, $fail) {
                    if (!User::where('phone', '=', $value)->exists()) {
                        $fail('Nomor Telepon yang Anda masukan tidak terdaftar !');
                    }
                },
            ],
            // 'password'          => 'required|min:8',
            // 'password_c'        => 'required|same:password'
        ],[
            'nik.required'         => 'NIK wajib diisi !',
            'phone.required'       => 'Nomor Telepon wajib diisi !',
            // 'password.required'      => 'Password wajib diisi !',
            // 'password.min'           => 'Password minimal harus 8 karakter kombinasi !',
            // 'password_c.required'    => 'Konfirmasi pasword wajib diisi !',
            // 'password_c.same'        => 'Konfirmasi pasword tidak sama !'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validate->errors(),
                'data'    => null
            ], 422);
        }

        $user = User::where('nik', '=', $request->nik)->orWhere('phone', '=', $request->phone)->first();

        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil, data cocok ditemukan !',
            'data'          => $user,
        ], 200);
    }

    public function reset(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'password'          => 'required|min:8',
            'password_c'        => 'required|same:password'
        ],[
            'password.required'      => 'Password wajib diisi !',
            'password.min'           => 'Password minimal harus 8 karakter kombinasi !',
            'password_c.required'    => 'Konfirmasi pasword wajib diisi !',
            'password_c.same'        => 'Konfirmasi pasword tidak sama !'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validate->errors(),
                'data'    => null
            ], 422);
        }

        $user = User::find($id);
        $user->password  = Hash::make($request->password);
        $user->save();

        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil mengubah password !',
            'data'          => $user,
        ], 200);
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();

        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil keluar !'
        ], 200);
    }
}
