<?php

namespace App\Http\Controllers\API;

use DB, Str, Storage, File, Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Validator;
use Image;
use Hash;

class ProfileController extends Controller
{
    public function getProfile($id)
    {
        $dataUser = User::where('id', $id);

        if ($dataUser->count() > 0) {
            return response()->json([
                'success'       => true,
                'message'       => 'Data berhasil ditemukan !',
                'data'          => $dataUser->get(),
            ], 200);
        }
        
        return response()->json([
            'success'       => false,
            'message'       => 'Data tidak ditemukan !',
            'data'          => null,
        ], 404);
    }

    public function updateProfile(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'nik'              => 'required',
            'name'             => 'required',
            'phone'            => 'required'
        ],[
            'nik.required'          => 'NIK wajib diisi !',
            'name.required'         => 'Nama Lengkap wajib diisi !',
            'phone.required'        => 'Nomor Telepon wajib diisi !',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validate->errors(),
                'data'    => null
            ], 422);
        }

        try {

            $userUpdate = User::find($id);
            $userUpdate->name           = $request->name;
            $userUpdate->phone          = $request->phone;
            $userUpdate->nik            = $request->nik;
            $userUpdate->name           = $request->name;
            $userUpdate->tempat_lahir   = $request->tempat_lahir;
            $userUpdate->tanggal_lahir  = $request->tanggal_lahir;
            $userUpdate->pekerjaan      = $request->pekerjaan;
            $userUpdate->provinsi       = $request->provinsi;
            $userUpdate->kabupaten_kota = $request->kabupaten_kota;
            $userUpdate->kecamatan      = $request->kecamatan;
            $userUpdate->alamat         = $request->alamat;
            $userUpdate->jenis_kelamin  = $request->jenis_kelamin;
            $userUpdate->golongan_darah = $request->golongan_darah;
            $userUpdate->save();

            return response()->json([
                'success'       => true,
                'message'       => 'Berhasil merubah data !',
                'data'          => $userUpdate,
            ], 200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function uploadPhoto(Request $request, $id){
        $validate = Validator::make($request->all(), [
            'photo'            => 'required|max:2048',
        ],[
            'photo.required'   => 'Photo wajib diisi !',
            'photo.max'        => 'Maksimal ukuran file 2 mb !',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validate->errors(),
                'data'    => null
            ], 422);
        }

        $path = public_path('profile/');

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }   

        $fileName = auth()->user()->photo;

        $photo = $request->file('photo');

        if ($request->hasFile('photo')) {
            $fileName = Str::random(15).'.'.$photo->getClientOriginalExtension();
            $photo->move($path, $fileName);

            $currentImage = auth()->user()->photo;

            if ($currentFile = file_exists($path.$currentImage)) {
                unlink($path.$currentImage);
            }
        }

        $user = User::find($id);
        $user->photo = $fileName;
        $user->save();

        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil mengunggah photo !',
            'data'          => $user,
        ], 200);
    }

    public function updatePassword(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'password_old' => [
                'required', function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail('Password lama tidak cocok !');
                    }
                },
            ],
            'password'          => 'required|min:8',
            'password_c'        => 'required|same:password'
        ],[
            'password_old.required'  => 'Password Lama wajib diisi !',
            'password.required'      => 'Password wajib diisi !',
            'password.min'           => 'Password minimal harus 8 karakter kombinasi !',
            'password_c.required'    => 'Konfirmasi pasword wajib diisi !',
            'password_c.same'        => 'Konfirmasi pasword tidak sama !'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Error',
                'errors'  => $validate->errors(),
                'data'    => null
            ], 422);
        }

        $user = User::find($id);
        $user->password  = Hash::make($request->password);
        $user->save();

        return response()->json([
            'success'       => true,
            'message'       => 'Berhasil mengubah password !',
            'data'          => $user,
        ], 200);
    }
}
