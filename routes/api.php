<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProfileController;

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('forgot', [AuthController::class, 'forgot']);
Route::post('reset/{id}', [AuthController::class, 'reset']);

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::get('/profile/{id}', [ProfileController::class, 'getProfile']);
    Route::post('/profile/update/{id}', [ProfileController::class, 'updateProfile']);
    Route::post('/profile/update-password/{id}', [ProfileController::class, 'updatePassword']);
    Route::post('/profile/upload-photo/{id}', [ProfileController::class, 'uploadPhoto']);

    Route::middleware(['role:pemohon'])->group(function () {

    });

    Route::middleware(['role:dokter'])->group(function () {

    });

    Route::post('/logout', [AuthController::class, 'logout']);
    
});
